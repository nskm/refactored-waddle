FROM python:3.11-slim as build
ENV DEBIAN_FRONTEND="noninteractive" TZ="Africa/Dakar"
RUN apt-get update && apt-get install -y --no-install-recommends texlive-latex-base texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra -y && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/ && rm -rf /var/cache/apt/

RUN pip install gunicorn flask pyvibe jinja2

COPY . .
RUN mkdir -p texfiles out
WORKDIR src/

EXPOSE 8000

CMD [ "gunicorn", "--bind", "0.0.0.0:8000", "app:app" ]
