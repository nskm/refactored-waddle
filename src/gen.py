#!/usr/bin/env python3
import subprocess
import pathlib
import jinja2
import os

here = pathlib.Path(__file__).parent

latex_jinja_env = jinja2.Environment(
    variable_start_string="\VAR{",
    loader=jinja2.FileSystemLoader(searchpath="../templates"),
)

# ask for the template
template = latex_jinja_env.get_template("template1.j2")

# ask for the content
content = {"student_name": "Patrick Nsukami", "level_name": "Lord of all the Rings"}

# finally, write the tex file
template.stream(content).dump("../texfiles/template1.tex")

# generate pdf file
process = subprocess.run(
    ["pdflatex", "../texfiles/template1.tex", "../out/template1.pdf"]
)
