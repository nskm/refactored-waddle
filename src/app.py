import shlex
import subprocess
import jinja2
from flask import Flask
import pyvibe as pv
import os
from flask import request, send_file, abort

app = Flask(__name__)


@app.route("/form", methods=["GET", "POST"])
def index():
    page = pv.Page("Form Example", navbar=None, footer=None)
    if request.method == "GET":
        with page.add_card() as card:
            card.add_header("Form Example")
            with card.add_form(action="/form", method="POST") as form:
                form.add_formtext(
                    label="Name", name="student_name", placeholder="Enter student name"
                )
                form.add_formtext(
                    label="Name", name="level_name", placeholder="Enter the level name"
                )
                form.add_formsubmit(label="Send")

        page.add_text(
            "PyPi analytics enables Python engineers to identify usage trends with Python packages as an input for picking the right package"
        )

        page.add_link("See the source code for this app", "#")

        page.add_text("")
        # render the form
        return page.to_html()
    else:
        student_name = request.form["student_name"]
        level_name = request.form["level_name"]
        tex_file_path = "../texfiles/" + student_name + "-certificate.tex"
        pdf_file_path = "../out/" + student_name + "-certificate.pdf"

        #
        latex_jinja_env = jinja2.Environment(
            variable_start_string="\VAR{",
            loader=jinja2.FileSystemLoader(searchpath=os.path.abspath("../templates")),
        )
        #
        # ask for the template
        template = latex_jinja_env.get_template("template1.j2")

        # ask for the content
        content = {"student_name": student_name, "level_name": level_name}

        # finally, write the tex file
        template.stream(content).dump(tex_file_path)

        # generate pdf file
        process = subprocess.run(
            args=[
                "pdflatex",
                "-output-directory",
                "../out",
                f"{tex_file_path}",
                f"{student_name}-certfificate.pdf",
            ],
            cwd=os.path.abspath("."),
        )

        try:
            return send_file(pdf_file_path, as_attachment=True)
        except FileNotFoundError:
            abort(404)
        finally:
            # delete all files
            pass


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
